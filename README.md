put\_blank\_lines.vim
=====================

This plugin provides mapping targets for inserting blank lines above or below
the current line without going into insert mode, including count prefixes and
repeating with the dot command.

The idea and insert method is lifted from the same mappings for
[unimpaired.vim][1], but called with `'operatorfunc'` to allow counts and
repeating without requiring [repeat.vim][2].

Configuration
-------------

To set mappings exactly like those from the original plugin, you might write
this in your `.vimrc`:

    nmap [<Space> <Plug>(PutBlankLinesAbove)
    nmap ]<Space> <Plug>(PutBlankLinesBelow)

License
-------

Copyright (c) [Tom Ryder][3].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://www.vim.org/scripts/script.php?script_id=1590
[2]: https://www.vim.org/scripts/script.php?script_id=2136
[3]: https://sanctum.geek.nz/
